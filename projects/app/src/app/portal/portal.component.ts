
import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';

import { GlobularMenuService } from 'lib-menu';

declare var $: any;


@Component({
    selector: 'app-portal',
    templateUrl: './portal.component.html',
    styleUrls: [
        './portal.component.scss',
        '../common/styles/side-bar.scss'
    ]
})

export class PortalComponent implements AfterViewInit {
    loading = true;
    toggle = {};

    @ViewChild('logout') elLogout: ElementRef;

    constructor(
        public router: Router,
        private menuService: GlobularMenuService
    ) {
        router.events.subscribe((routerEvent: Event) => {
            this.checkRouterEvent(routerEvent);
        });

        this.toggle = menuService.toggle;
    }


    ngAfterViewInit() {
        $(this.elLogout.nativeElement).dropdown();
    }


    public checkRouterEvent(routerEvent: Event): void {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        }

        if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }
}
