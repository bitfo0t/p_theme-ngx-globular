import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortalComponent } from './portal.component';

const routes: Routes = [{
  path: '',
  component: PortalComponent,
  children: [{
    path: '',
    redirectTo: 'a',
    pathMatch: 'full'
  }, {
    path: 'a',
    loadChildren: () => import('./../portal-a/portal-a.module').then(m => m.PortalAModule)
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PortalRoutingModule { }
