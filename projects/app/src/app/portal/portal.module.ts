
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlobularMenuModule, GlobularMenuService } from 'lib-menu';


import { PortalRoutingModule } from './portal-routing.module';
import { PortalComponent } from './portal.component';




@NgModule({
    declarations: [
        PortalComponent
    ],
    imports: [
        CommonModule,
        PortalRoutingModule,
        GlobularMenuModule
    ],
    exports: [
        PortalComponent,
    ],
    providers: [GlobularMenuService]
})
export class PortalModule { }
