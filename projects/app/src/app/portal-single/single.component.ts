
import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-portal-single',
    templateUrl: './single.component.html',
    styleUrls: [
        './single.component.scss',
        './../common/styles/side-menu.scss'
    ]
})

export class SingleComponent implements OnInit {
    constructor() { }


    ngOnInit(): void {
        this.changeTheme('#06d6a0');
    }


    private changeTheme(primary: string) {
        document.documentElement.style.setProperty('--primary-color', primary);
    }
}
