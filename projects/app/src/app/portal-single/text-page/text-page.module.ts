
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextPageRoutingModule } from './text-page-routing.module';
import { TextPageComponent } from './text-page.component';


@NgModule({
  declarations: [
    TextPageComponent
  ],
  imports: [
    CommonModule,
    TextPageRoutingModule
  ]
})
export class TextPageModule { }
