import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleComponent } from './single.component';

const routes: Routes = [{
  path: '',
  component: SingleComponent,
  children: [{
    path: 'text-page',
    loadChildren: () => import('./text-page/text-page.module').then(m => m.TextPageModule)
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SingleRoutingModule { }
