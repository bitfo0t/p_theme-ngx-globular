
import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [
        './login.component.scss',
        './../common/styles/col-split.scss'
    ]
})
export class LoginComponent implements OnInit {
    formName = {
        signIn: {
            status: true,
            slide: 'left'
        },
        signUp: {
            status: false,
            slide: 'right'
        },
        forgotPassword: {
            status: false,
            slide: 'right'
        },
    };

    slideAction = {
        'slide-right': false,
        'slide-left': false
    };

    constructor() { }


    ngOnInit(): void {
    }


    formSelector(fn) {
        Object.keys(this.formName).forEach(key => {
            console.log(key)
            this.formName[key].status = false;
        });

        this.formName[fn].status = true;

        this.slideAction = {
            'slide-right': false,
            'slide-left': false
        };

        this.slideAction['slide-' + this.formName[fn].slide] = true;
    }
}
