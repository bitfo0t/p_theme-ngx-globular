import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '',
  children: [{
    path: '',
    loadChildren: () => import('./portal/portal.module').then(m => m.PortalModule)
  }, {
    path: 'b',
    loadChildren: () => import('./portal-single/single.module').then(m => m.SingleModule)
  }, {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  }],
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
