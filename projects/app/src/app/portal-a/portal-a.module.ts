
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlobularMenuModule } from 'lib-menu';

import { PortalARoutingModule } from './portal-a-routing.module';
import { PortalAComponent } from './portal-a.component';


@NgModule({
    declarations: [
        PortalAComponent,
    ],
    imports: [
        CommonModule,
        PortalARoutingModule,
        GlobularMenuModule
    ]
})

export class PortalAModule {}
