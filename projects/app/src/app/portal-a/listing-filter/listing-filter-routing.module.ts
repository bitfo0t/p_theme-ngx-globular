
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListingFilterComponent } from './listing-filter.component';

const routes: Routes = [{
  path: '',
  component: ListingFilterComponent,
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ListingFilterRoutingModule { }
