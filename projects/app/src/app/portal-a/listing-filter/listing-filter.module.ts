
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';

import { ListingFilterRoutingModule } from './listing-filter-routing.module';
import { ListingFilterComponent } from './listing-filter.component';


@NgModule({
  declarations: [
    ListingFilterComponent
  ],
  imports: [
    CommonModule,
    ListingFilterRoutingModule,
    NgxDatatableModule,
    NgSelectModule
  ]
})
export class ListingFilterModule { }
