
import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-listing-single',
    templateUrl: './listing-single.component.html',
    styleUrls: [
        './listing-single.component.scss',
        './../../../common/styles/tab-header.scss'
    ]
})
export class ListingSingleComponent implements OnInit {

    constructor() { }


    ngOnInit() {
    }
}
