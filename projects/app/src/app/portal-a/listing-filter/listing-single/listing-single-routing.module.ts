
import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListingSingleComponent } from './listing-single.component';

const routes: Routes = [{
    path: '',
    component: ListingSingleComponent
}];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
    ]
})

export class ListingSingleRoutingModule {}
