
import { NgModule } from '@angular/core';
import { ListingSingleRoutingModule } from './listing-single-routing.module';
import { ListingSingleComponent } from './listing-single.component';


@NgModule({
    imports: [
        ListingSingleRoutingModule,
    ],
    declarations: [
        ListingSingleComponent,
    ],
    exports: [
        ListingSingleComponent,
    ],
    providers: [
    ],
})
export class ListingSingleModule { }
