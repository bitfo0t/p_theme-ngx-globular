
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';


declare var $: any;

@Component({
    selector: 'app-listing-filter',
    templateUrl: './listing-filter.component.html',
    styleUrls: [
        './listing-filter.component.scss', 
        './../../common/styles/top-header.scss',
        './../../common/styles/ngx-table.scss', 
        './../../common/styles/filter.scss'
    ]
})
export class ListingFilterComponent implements OnInit, AfterViewInit {
    rows = [];
    datePickerFrom: any;
    datePickerTill: any;
    createdDatePickerFrom: any;
    createdDatePickerTill: any;

    @ViewChild('datePickerFrom') elDatePickerFrom: ElementRef;
    @ViewChild('datePickerTill') elDatePickerTill: ElementRef;
    @ViewChild('createdDatePickerFrom') elCreatedDatePickerFrom: ElementRef;
    @ViewChild('createdDatePickerTill') elCreatedDatePickerTill: ElementRef;


    constructor() { }


    ngOnInit(): void {
        let obj = {
            id: 1,
            name: 'John Smith',
            email: 'johnsmith@gmail.com',
            status: 'active',
            created: 'Nov 07, 2018',
            updated: 'May 01, 2020',
            lastActive: 'May 01, 2020'
        };

        for (let index = 0; index < 30; index++) {
            let objTemp = JSON.parse(JSON.stringify(obj))
            objTemp.id = 1 + index;
            this.rows.push(objTemp)
        }
    }


    ngAfterViewInit(): void {
        this.datePickerFrom = $(this.elDatePickerFrom.nativeElement).datepicker({
            autoClose: true,
            inline: false,
            classes: 'datepicker-date-range',
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            onSelect: (value, date, inst) => {
            }
        }).data('datepicker');

        this.datePickerTill = $(this.elDatePickerTill.nativeElement).datepicker({
            autoClose: true,
            inline: false,
            classes: 'datepicker-date-range',
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            onSelect: (value, date, inst) => {
            }
        }).data('datepicker');

        this.createdDatePickerFrom = $(this.elCreatedDatePickerFrom.nativeElement).datepicker({
            autoClose: true,
            inline: false,
            classes: 'datepicker-created-date-range',
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            onSelect: (value, date, inst) => {
            }
        }).data('datepicker');

        this.createdDatePickerTill = $(this.elCreatedDatePickerTill.nativeElement).datepicker({
            autoClose: true,
            inline: false,
            classes: 'datepicker-created-date-range',
            language: 'en',
            dateFormat: 'yyyy-mm-dd',
            onSelect: (value, date, inst) => {
            }
        }).data('datepicker');
    }
}