
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortalAComponent } from './portal-a.component';

const routes: Routes = [{
  path: '',
  component: PortalAComponent,
  children: [{
    path: 'listing-filter',
    loadChildren: () => import('./listing-filter/listing-filter.module').then(m => m.ListingFilterModule)
  },{
    path: 'listing-filter/:id',
    loadChildren: () => import('./listing-filter/listing-single/listing-single.module').then(m => m.ListingSingleModule)
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PortalARoutingModule { }
