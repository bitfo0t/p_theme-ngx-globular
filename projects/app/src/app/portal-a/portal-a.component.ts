
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { Subscription } from 'rxjs';
import { GlobularMenuService } from 'lib-menu';

declare var $: any;


@Component({
    selector: 'app-portal-a',
    templateUrl: './portal-a.component.html',
    styleUrls: [
        './portal-a.component.scss',
        './../common/styles/side-menu.scss'
    ]
})

export class PortalAComponent implements OnInit, AfterViewInit, OnDestroy {
    toggle = {};
    subscription: Subscription;

    @ViewChild('examsSubmenu') elExamsSubmenu: ElementRef;

    constructor(
        private menuService: GlobularMenuService
    ) {
        this.toggle = menuService.toggle;

        this.subscription = menuService.getToggle.subscribe(toggle => {
            this.toggle = toggle;
        });
    }


    ngOnInit(): void {
        this.changeTheme('#0f92f3');
    }


    ngAfterViewInit() {
        $(this.elExamsSubmenu.nativeElement)
            .accordion();
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


    private changeTheme(primary: string) {
        document.documentElement.style.setProperty('--primary-color', primary);
    }
}
