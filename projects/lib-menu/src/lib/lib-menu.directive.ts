
import { Directive, HostListener, Input, ElementRef } from '@angular/core';

import { GlobularMenuService } from './lib-menu.service';


@Directive({
    selector: '[appMenu]'
})

export class GlobularMenuDirective {
    @Input('appMenu') toggle = {};
    @Input() enter = false;
    @Input() click = false;


    constructor(
        private menuService: GlobularMenuService,
        private el: ElementRef
    ) {}


    @HostListener('mouseenter') onMouseEnter() {
        if (!this.toggle['open'] && !this.toggle['hover'] && this.enter) {
            this.toggleHandler('hover', true);
        }
    }


    @HostListener('mouseleave') onMouseLeave() {
        if (!this.toggle['open'] && this.toggle['hover'] && !this.enter) {
            this.toggleHandler('hover', false);
        }
    }


    @HostListener('click') onClick() {
        if (this.click) {
            if (this.toggle['open']) {
                this.toggleHandler('open', false);
            } else {
                this.toggleHandler('open', true);
                this.el.nativeElement.style.display = 'none';

                setTimeout(() => {
                    this.el.nativeElement.style.display = 'block';
                }, 700)
            }
        }
    }


    toggleHandler(key: string, value: boolean) {
        this.menuService.setToggle({ key: key, value: value });
    }
}
