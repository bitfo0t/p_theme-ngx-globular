
import { NgModule } from '@angular/core';

import { GlobularMenuDirective } from './lib-menu.directive';


@NgModule({
    declarations: [
        GlobularMenuDirective
    ],
    exports: [
        GlobularMenuDirective
    ]
})

export class GlobularMenuModule { }
