
import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})

export class GlobularMenuService {
    private toggleReturn = new Subject<object>();
    getToggle = this.toggleReturn.asObservable();
    toggle = {
        'hover': false,
        'open': true
    };


    setToggle(event: any) {
        this.toggle[event.key] = event.value;

        if (this.toggle['open']) {
            this.toggle['hover'] = false;
        }

        this.toggleReturn.next(this.toggle);
    }
}
